export enum Operator {
    INCREMENT = 'INCREMENT',
    DECREMENT = 'DECREMENT'
}