import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpComponent } from './pages/up/up.component';
import { DownComponent } from './pages/down/down.component';
import { ResetComponent } from './pages/reset/reset.component';

const routes: Routes = [
  { path: 'up', component: UpComponent },
  { path: 'down', component: DownComponent },
  { path: 'reset', component: ResetComponent },
  { path: '', redirectTo: '/up', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
