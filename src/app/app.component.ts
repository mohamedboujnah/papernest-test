import { Component } from '@angular/core';
import { CounterService } from './services/counter.service';

export enum Colors {
  RED = '#e74c3c',
  GREEN = '#27ae60'
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  public counter = 0;
  public backgroundColor = '';

  constructor(private counterService: CounterService) {}

  ngOnInit() {
    this.getCounter();
  }

  private getCounter(): void {
    this.counterService.counterSubject.subscribe(counter => {
      this.counter = counter;
      this.updateBackgroundColor();
    });
  }

  private updateBackgroundColor() {
    if (this.counter === 10) {
      this.backgroundColor = Colors.RED;
    } else if (this.counter === -10) {
      this.backgroundColor = Colors.GREEN;
    } else {
      this.backgroundColor = '';
    }
  }
}
