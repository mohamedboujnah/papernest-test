import { Component } from '@angular/core';
import { CounterService } from 'src/app/services/counter.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent {
  public birthdate: string = '';

  constructor(private counterService: CounterService) {}
  /**
   * reset counter if the age > 18
   */
  public resetCounter() {
    const currentDate = new Date();
    const selectedDate = new Date(this.birthdate);

    if ((currentDate.getFullYear() - selectedDate.getFullYear()) >= 18) {
      this.counterService.resetCounter();
    }
  }
}
