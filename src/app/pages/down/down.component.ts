import { Component } from '@angular/core';
import { Operator } from 'src/app/enums/operator.enum';
import { CounterService } from 'src/app/services/counter.service';

@Component({
  selector: 'app-down',
  templateUrl: './down.component.html',
  styleUrls: ['./down.component.scss']
})
export class DownComponent {

  constructor(private counterService: CounterService) {}

  public updateCounter() {
    const currentValue = this.counterService.getValue();
    this.counterService.updateCounter(currentValue, Operator.DECREMENT);
  }
}
