import { Component } from '@angular/core';
import { Operator } from 'src/app/enums/operator.enum';
import { CounterService } from 'src/app/services/counter.service';

@Component({
  selector: 'app-up',
  templateUrl: './up.component.html',
  styleUrls: ['./up.component.scss']
})
export class UpComponent {
  constructor(private counterService: CounterService) {}
  
  public updateCounter() {
    const currentValue = this.counterService.getValue();
    this.counterService.updateCounter(currentValue, Operator.INCREMENT);
  }
}
