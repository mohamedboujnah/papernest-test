import { TestBed } from '@angular/core/testing';

import { CounterStorageService } from './counter-storage.service';

describe('CounterStorageService', () => {
  let service: CounterStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CounterStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
