import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CounterStorageService } from './counter-storage.service';
import { Operator } from '../enums/operator.enum';

@Injectable({
  providedIn: 'root'
})
export class CounterService {

  public counterSubject = new BehaviorSubject<number>(this.counterStorageService.loadCounter() || 0);
  private totalActions = 0;
  private actionsCount = 30;
  private incrementBy = this.counterStorageService.loadIncrementBy() || 1;

  constructor(private counterStorageService: CounterStorageService) {}

  /**
   * main function to update the counter
   */
  public updateCounter(value: number, operator: Operator): void {
    this.totalActions++;
    if(operator === Operator.INCREMENT) {
      value += this.incrementBy;
    } else {
      value -= this.incrementBy;
    }

    // multiply by 2 the incrementBy (X) every 30 action
    if (this.totalActions === this.actionsCount) {
      this.totalActions = 0;
      this.incrementBy++;
      this.counterStorageService.saveIncrementBy(this.incrementBy);
    }

    this.counterSubject.next(value);
    this.counterStorageService.saveCounter(value);
  }

  /**
   * resetting the counter and incrementBy
   */
  public resetCounter(): void {
    this.counterSubject.next(0);
    this.incrementBy = 1;
    this.counterStorageService.clearCounter();
  }

  /**
   * get counter value 
   */
  public getValue(): number {
    return this.counterSubject.value;
  }

}
