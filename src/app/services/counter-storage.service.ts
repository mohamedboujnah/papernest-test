import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterStorageService {

  constructor() { }

  saveCounter(value: number): void {
    localStorage.setItem('counter', value.toString());
  }

  saveIncrementBy(value: number): void {
    localStorage.setItem('incrementBy', value.toString());
  }

  loadCounter(): number | null {
    const storedValue = localStorage.getItem('counter');
    return storedValue ? parseInt(storedValue) : null;
  }

  loadIncrementBy(): number | null {
    const storedValue = localStorage.getItem('incrementBy');
    return storedValue ? parseInt(storedValue) : null;
  }

  clearCounter(): void {
    localStorage.removeItem('counter');
    localStorage.removeItem('incrementBy');
  }
}
